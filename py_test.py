## import urllib
import sys
import requests
import pytest

link = "https://the-internet.herokuapp.com/context_menu"  

##this is the first Test - Pass if "Right click.." string is found in url
def test_find_string():
    ## this is HTTP GET request
    response = requests.get(link)

    ##checks if fetching the url was successfull = 200
    ##if response.status_code == 200:
      ##print(response.text)
    print("Before the condition")
    if response.status_code == 200:
        print("Status code is 200. Content:")
        print(response.text)
    else:
        print("Status code is not 200:", response.status_code)
    print("After the condition")

    find_string = "Right-click in the box below to see one called 'the-internet'"  
    
    assert find_string in response.text, f"'{expected_string}' not found on the page."


if __name__ == "__main__":
  test_find_string()

